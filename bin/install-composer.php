<?php
copy('https://getcomposer.org/installer', 'composer-setup.php');
if(hash_file('SHA384', 'composer-setup.php') === 'e115a8dc7871f15d853148a7fbac7da27d6c0030b848d9b3dc09e2a0388afed865e6a3d6b3c0fad45c48e2b5fc1196ae')
{
	echo 'Installer verified';
} 
else
{ 
	echo 'Installer corrupt';
	unlink('composer-setup.php');
}
echo PHP_EOL;
//***************************************************

system('php composer-setup.php');
if(!file_exists('composer.phar'))
{
	echo "Looks like composer failed to install correctly. Exiting...";
	exit(666);
}
//***************************************************

$res = unlink('composer-setup.php');
if(!$res)
{
	echo "Could not delete composer-setup.php";
	print_r( error_get_last() );
	exit(666);
}
