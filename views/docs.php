<div class="text">
	<h1>Documentation</h1>

	<div>
		<p>
			I hope to achieve PHP framework independence in that the business logic should reside completely 
			separate from the rest of the code, and it shouldn't matter if I use CI or the Prashu
			framework (which I am just starting by the way) or even more modern frameworks like laravel and so on.
		</p>
		
		<br/>
		
		<p>
		I have a few goals in mind, which are as follows:
			<ol>
				<li>Separation of Business Logic(as much as possible)</li>
				<li>Sanitization should be a breeze & not a chore</li>
				<li>ORM should be in-built</li>
				<li>Adding another conceptual layer between client and server via JS objects(kind of like JS MVC)</li>
			</ol>
		</p>

		<p>
		Desirable goals:
			<ol>
				<li>Support distributed file systems & make installation very easy.</li>
				<li>Support mailgun by default</li>
				<li>Have some type of support for background processes</li>
				<li>Play nice with message queuing servers like AMQ</li>
				<li>Have a good library for interacting with Payment Gateways like STRIPE</li>
			</ol>
		</p>
	</div>
	
	<div>
		<p>The rest of the things I will add as I go along. For now, this seems like a pretty good starting point.
		</p>
	</div>
	<hr/>
	
	<div>
		<h2>The Actual Documentation</h2>
		
		<strong>General</strong>
		<ul>
			<li>CONSTANTS: Constants can be rendered inside views. Follow the same syntax as variables.</li>
			<li>VARIABLES: Use {varName} to render variables inside views.</li>
		</ul>
		
		<strong>Keywords</strong>
		<ul>
			<li>PAGE_TITLE: Reserved for the page title.</li>
			<li>_TEMPLATE_PAGE_CONTENT: Used when rendering the child view inside the main view.</li>
		</ul>
	</div>
</div>
