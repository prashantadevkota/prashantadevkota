<div class="text">
	<h1>Source Code</h1>

	<div>
		<p>
			The repository for this website is public.
			You can clone the repository using the below methods:
		</p>
		
		<table class="table1">
			<thead>
			<tr>
				<td><strong>Protocol</strong></td>
				<td><strong>String</strong></td>
			</tr>
			</thead>
			
			<tbody>
			<tr>
				<td>SSH</td>
				<td>git@bitbucket.org:prashantadevkota/prashantadevkota.git</td>
			</tr>
			<tr>
				<td>HTTPS</td>
				<td>https://bitbucket.org/prashantadevkota/prashantadevkota.git</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
