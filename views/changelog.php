<hr/>
<strong>v1.0.0 Features	09-Sep-2016</strong>

1. Added parameter passing to controller functions
2. Added the 'view' display & access to passed data
3. Added return as text to the 'view' display.
<hr/>

