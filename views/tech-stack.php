<div class="text">
	<h1>Technology Stack</h1>

	<div>
		<p>
			I am using the below technologies for running this website.
		</p>
		
		<table class="table2">
			<thead>
			<tr>
				<td><strong>Stack</strong></td>
				<td><strong>Description</strong></td>
			</tr>
			</thead>
			
			<tbody>
			<tr>
				<td>HTML5</td>
				<td>HTML version 5</td>
			</tr>
			<tr>
				<td>CSS3</td>
				<td>Cascading Style Sheets</td>
			</tr>
						<tr>
				<td>LAMP</td>
				<td>OS, Webserver, Database, Language</td>
			</tr>
			<tr>
				<td>Prashu Framework</a></td>
				<td>Custom PHP Framework</td>
			</tr>
			<tr>
				<td><a href="https://aws.amazon.com" target="_new">Amazon EC2</a></td>
				<td>Cloud computing</td>
			</tr>
			<tr>
				<td><a href="https://bitbucket.org" target="_new">Bitbucket</a></td>
				<td>Version Control</td>
			</tr>
			<tr>
				<td><a href="http://getbootstrap.com/" target="_new">Bootstrap</a></td>
				<td>Frontend Framework</td>
			</tr>
			<tr>
				<td><a href="https://getcomposer.org" target="_new">Composer</a></td>
				<td>Dependency Manager for PHP</td>
			</tr>
			<tr>
				<td><a href="https://fonts.google.com" target="_new">Google Fonts</a></td>
				<td>Font library maintained by Google(The mother of the web)</td>
			</tr>
			<tr>
				<td><a href="https://jquery.com" target="_new">jQuery</a></td>
				<td>JavaScript Library</td>
			</tr>
			<tr>
				<td><a href="https://letsencrypt.org" target="_new">Let's Encrypt</a></td>
				<td>Free &amp; self-signed SSL Certificates for the HTTPS protocol</td>
			</tr>
			<tr>
				<td><a href="https://www.codeigniter.com" target="_new">CodeIgniter 2</a></td>
				<td>PHP Framework No. 2</td>
			</tr>
			</tbody>
		</table>
	</div>
	
	<p>&nbsp;</p>
	
	<div>
		<p>
			Upcoming technologies.
		</p>
		
		<table class="table2">
			<thead>
			<tr>
				<td><strong>Stack</strong></td>
				<td><strong>Description</strong></td>
			</tr>
			</thead>
			
			<tbody>
			<tr>
				<td><a href="https://bower.io/">Bower</a></td>
				<td>Frontend Dependency Manager</td>
			</tr>
			<tr>
				<td><a href="https://angularjs.org/" target="_new">Angular JS</a></td>
				<td>Javascript MVC Framework</td>
			</tr>
			<tr>
				<td><a href="https://www.docker.com/" target="_new">Docker</a></td>
				<td>Software Containerization Platform</td>
			</tr>
			<tr>
				<td><a href="https://www.mailgun.com" target="_new">Mailgun</a></td>
				<td>Email library/service</td>
			</tr>
			</tbody>
			</table>
		</div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
