<div class="text">
	<h1>About Me</h1>

	<div>
		<p>
			Hi I'm Prashanta Prasad Devkota and welcome to my site.
		</p>
	</div>
</div>

<div class="padded">
	<div class="float-left">
		<img alt="Prashanta P. Devkota" title="Prashanta P. Devkota" src="{CDN_URL}/images/prashanta-devkota-small.jpg">
		<div style="text-align: center;">Prashanta P. Devkota</div>
	</div>
	
	<div class="float-left padded" style="width: 200px;">
		<p>
			Originally from Nepal, I am a permanent resident of Australia and 
			currently reside in Melbourne after staying in Sydney for about 5-6 months.
		</p>
	</div>
	
	<div class="clear-both">&nbsp;</div>
</div>

<div class="text">
	<h3>Interests</h3>
	<ul class="list-simple">
		<li>Acoustic Guitar</li>
		<li>Chess</li>
		<li>Programming</li>
		<li>Role Playing Games</li>
		<li>Vedic Astrology</li>
	</ul>
</div>
