<div class="text">
	<h1>My Resume</h1>

	<div>
		<p>
			Please choose your format below:
		</p>
		
		<table class="table2">
			<thead>
			<tr>
				<td><strong>Resume</strong></td>
				<td><strong>File Type</strong></td>
			</tr>
			</thead>
			
			<tbody>
			<tr>
				<td>
					<a href="/main/download/resume-docx">
						<img src="/images/docx-64.png" class="thumbnail-tiny-square"/>
					</a>
				</td>
				<td>Word Document(docx)</td>
			</tr>
			<tr>
				<td>
					<a href="/main/download/resume-pdf">
						<img src="/images/pdf-64.jpg" class="thumbnail-tiny-square"/>
					</a>
				</td>
				<td>PDF</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
