<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="author" content="Prashanta P. Devkota"/>

    <!-- OG Tags -->
	<meta property="og:title" content="Prajna's Rice Feeding" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://prashantadevkota.com/prajna" />
	<meta property="og:site_name" content="PrashantaDevkota.com" />
	<meta property="og:description" content="Prajna ko paasni" />
	<meta property="og:image" content="https://prashantadevkota.com/images/prajna/paasni-xlarge.jpg" />    
    <!-- END_OF OG Tags -->
    
    <title>{PAGE_TITLE}::Prashanta P. Devkota</title>

    <!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Corben" rel="stylesheet">
	
    <!-- Fav Icon -->
    <link href="{CDN_URL}/favicon.png{PRASHU_QSTRING}" rel="icon" type="image/png">
    
    <!-- Bootstrap -->
    <link href="{CDN_URL}/bootstrap/css/bootstrap.min.css{BOOTSTRAP_QSTRING}" rel="stylesheet">
    <link href="{CDN_URL}/bootstrap/css/bootstrap-theme.min.css{BOOTSTRAP_QSTRING}" rel="stylesheet">

    <!--  Various Plugins -->
    <link href="{CDN_URL}/css/animate.css" rel="stylesheet">
    <link href="{CDN_URL}/css/font-icons.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    
    <!-- Prashu Stuff -->
    <!-- 
	    Extra small devices Phones (<768px)
		Small devices Tablets (≥768px)
		Medium devices Desktops (≥992px)
		Large devices Desktops (≥1200px)
     -->
    <link href="{CDN_URL}/css/global.css{PRASHU_QSTRING}" rel="stylesheet">
<!--     <link href="{CDN_URL}/css/screen-large.css{PRASHU_QSTRING}" media="(min-width: 1200px)" rel="stylesheet"> -->
<!--     <link href="{CDN_URL}/css/screen-medium.css{PRASHU_QSTRING}" media="(min-width: 992px)" rel="stylesheet"> -->
<!--     <link href="{CDN_URL}/css/screen-small.css{PRASHU_QSTRING}" media="(min-width: 768px)" rel="stylesheet"> -->
    <link href="{CDN_URL}/css/screen-xsmall.css{PRASHU_QSTRING}" media="(max-width: 768px)" rel="stylesheet">
    <!-- END_OF Prashu Stuff -->
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
     <!-- JS Scripts -->
    <script type="text/javascript" src="{CDN_URL}/jquery/jquery.min.js{JQUERY_QSTRING}"></script>
    <script type="text/javascript" src="{CDN_URL}/bootstrap/js/bootstrap.min.js{BOOTSTRAP_QSTRING}"></script>
    
    <!-- Prashanta Scripts -->
    <script type="text/javascript" src="{CDN_URL}/js/global.js{PRASHU_QSTRING}"></script>
     <!-- END_OF JS Scripts -->
  </head>
  
  <body>
    <section>
		<!-- The Content -->
    	<div id="content">
    		{_TEMPLATE_PAGE_CONTENT}
    	</div> <!-- END_OF The Content -->
    </section>
  </body>
</html>