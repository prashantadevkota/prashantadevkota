<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{PAGE_TITLE}::Prashanta P. Devkota</title>

    <!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Corben" rel="stylesheet">
	
    <!-- Fav Icon -->
    <link href="{CDN_URL}/favicon.png{PRASHU_QSTRING}" rel="icon" type="image/png">
    
    <!-- Bootstrap -->
    <link href="{CDN_URL}/bootstrap/css/bootstrap.min.css{BOOTSTRAP_QSTRING}" rel="stylesheet">
    <link href="{CDN_URL}/bootstrap/css/bootstrap-theme.min.css{BOOTSTRAP_QSTRING}" rel="stylesheet">

    <!--  Various Plugins -->
    <link href="{CDN_URL}/css/animate.css" rel="stylesheet">
    <link href="{CDN_URL}/css/font-icons.css" rel="stylesheet">
    
    <!-- Prashu Stuff -->
    <!-- 
	    Extra small devices Phones (<768px)
		Small devices Tablets (≥768px)
		Medium devices Desktops (≥992px)
		Large devices Desktops (≥1200px)
     -->
    <link href="{CDN_URL}/css/global.css{PRASHU_QSTRING}" rel="stylesheet">
<!--     <link href="{CDN_URL}/css/screen-large.css{PRASHU_QSTRING}" media="(min-width: 1200px)" rel="stylesheet"> -->
<!--     <link href="{CDN_URL}/css/screen-medium.css{PRASHU_QSTRING}" media="(min-width: 992px)" rel="stylesheet"> -->
<!--     <link href="{CDN_URL}/css/screen-small.css{PRASHU_QSTRING}" media="(min-width: 768px)" rel="stylesheet"> -->
    <link href="{CDN_URL}/css/screen-xsmall.css{PRASHU_QSTRING}" media="(max-width: 768px)" rel="stylesheet">
    <!-- END_OF Prashu Stuff -->
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
     <!-- JS Scripts -->
    <script type="text/javascript" src="{CDN_URL}/jquery/jquery.min.js{JQUERY_QSTRING}"></script>
    <script type="text/javascript" src="{CDN_URL}/bootstrap/js/bootstrap.min.js{BOOTSTRAP_QSTRING}"></script>
    
    <!-- Prashanta Scripts -->
    <script type="text/javascript" src="{CDN_URL}/js/global.js{PRASHU_QSTRING}"></script>
     <!-- END_OF JS Scripts -->
  </head>
  
  <body>
    <!-- Navigation Header -->
	<nav class="navbar navbar-default">
		<header>
			<div class="container-fluid">
				<div class="prashu-nav">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="logo">
							<a href="/"><img class="logo-img" src="/images/prashu-logo-3.png" title="Prashanta Prasad Devkota" /></a>
							<span class="logo-text1">PHP Developer</span>
						</div>
					</div>
		
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li class=""><a href="/main/resume">Resume</a></a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									Tech-Stuff <span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="/main/docs">Docs</a></a></li>
									<li role="separator" class="divider"></li>
									<li><a href="/main/source">Source</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="/main/techstack">Tech-Stack</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="/webconsole">Web-console</a></li>
									<li role="separator" class="divider"></li>
								</ul>
							</li>
							<li><a href="/main/about">About</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				
				</div><!-- /.prashu-nav -->
			</div><!-- /.container-fluid -->
		</header>
    </nav>
    <!-- END_OF Navigation Header -->
    
    <section>
		<!-- The Content -->
    	<div id="content">
    		{_TEMPLATE_PAGE_CONTENT}
    	</div> <!-- END_OF The Content -->

		<footer>
			<div id="footer" class="dark">
				<div id="copyright">
					<div class="float-left">
						Copyright &copy; 2016 All Rights Reserved by Prashanta Prasad Devkota
						<br/>
						<a href="#">Terms of Use</a> | 
						<a href="#">Privacy Policy</a>
					</div>
					
					<div class="float-right">
                        <div>
                            <a href="#" class="social-icon si-small si-borderless si-facebook" data-social-name="fb">
                                <i class="icon-facebook icon-block"></i>
                                <i class="icon-facebook icon-block"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-twitter" data-social-name="twitter">
                                <i class="icon-twitter icon-block"></i>
                                <i class="icon-twitter icon-block"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-linkedin" data-social-name="linkedin">
                                <i class="icon-linkedin icon-block"></i>
                                <i class="icon-linkedin icon-block"></i>
                            </a>
                            
                            <a href="#" class="social-icon si-small si-borderless si-github" data-social-name="github">
                                <i class="icon-github icon-block"></i>
                                <i class="icon-github icon-block"></i>
                            </a>
                            
                            <a href="#" class="social-icon si-small si-borderless si-gplus" data-social-name="gplus">
                                <i class="icon-gplus icon-block"></i>
                                <i class="icon-gplus icon-block"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-pinterest" data-social-name="pinterest">
                                <i class="icon-pinterest icon-block"></i>
                                <i class="icon-pinterest icon-block"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-vimeo" data-social-name="vimeo">
                                <i class="icon-vimeo icon-block"></i>
                                <i class="icon-vimeo icon-block"></i>
                            </a>
                        </div>
                        <div class="clear-both">&nbsp;</div>
                        
                        <i class="action icon-envelope2" data-action="email" data-action-val="prashantadevkota@hotmail.com"></i>
                        <span class="action" data-action="email" data-action-val="prashantadevkota@hotmail.com">prashantadevkota@hotmail.com</span>
                        <span>|</span>
                        <i class="action icon-phone" data-action="mobile" data-action-val="+61 422 941227"></i>
                        <span class="action margin-left-neg-4" data-action="mobile" data-action-val="+61422941227">+61-422-941227</span>
                        <span>|</span>
                        <i class="action icon-skype2" data-action="skype" data-action-val="prashantadevkota"></i>
                        <span class="action" data-action="skype" data-action-val="prashantadevkota">PrashantaDevkota</span>
					</div>
					
<!-- 					<div class="clear-both">&nbsp;</div> <p>&nbsp;</p> <p>&nbsp;</p> -->
				</div>
			</div>
		</footer>
    </section>
    
  </body>
</html>