<link href="{CDN_URL}/css/carousel.css{CAROUSEL_QSTRING}" rel="stylesheet">

<div id="landing-carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#landing-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#landing-carousel" data-slide-to="1" class=""></li>
          <li data-target="#landing-carousel" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="images/birds-in-sky.jpg" alt="First slide: Birds In the Sky" data-holder-rendered="true">
          </div>
          <div class="item">
            <img src="images/sunset1.jpg" alt="Second slide: Sunset Picture1" data-holder-rendered="true">
          </div>
          <div class="item">
            <img src="images/sunset2.jpg" alt="Third slide: Sunset Picture2" data-holder-rendered="true">
          </div>
        </div>
        <a class="left carousel-control" href="#landing-carousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#landing-carousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
</div>

<div id="animation_settings">
	<h5>Animation Settings</h5>
	
	<table class="settings-table">
		<thead>
			<tr>
				<td>Name</td>
				<td>Value</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Number of Balloons</td>
				<td> <input id="sldNumBalloons" type="range" min="9" max="27" step="1" /> </td>
			</tr>
			<tr>
				<td>Animation Smoothness</td>
				<td> <input id="sldAnimationSmoothness" type="range" min="1" max="10" step="1" /> </td>
			</tr>
		</tbody>
	</table>
	<div style="color: #ff0097; font-weight: bold;">Under construction. Please check back!</div>
</div>

<div id="fold_arrow">
	<i class="action grow fa fa-chevron-circle-down" data-action="goBelowFold" data-actionval="goBelowFold"></i>
</div>

<div id="animation_control">
	<i class="action fa fa-step-forward" title="Next Frame (n)" data-action="nextFrame" data-actionval="nextFrame"></i>
	<i class="action fa fa-play-circle-o" title="Play (p)" data-action="playAnimation" data-actionval="playAnimation"></i>
	<i class="action fa fa-pause-circle-o" title="Pause (u)" data-action="pauseAnimation" data-actionval="pauseAnimation"></i>
	<i class="action fa fa-stop-circle-o" title="Stop (o)" data-action="stopAnimation" data-actionval="stopAnimation"></i>
	<i class="action fa fa-step-backward" title="Previous Frame (Under Construction) (v)" data-action="previousFrame" data-actionval="previousFrame"></i>
	<i class="action fa fa-cog" title="Settings (Under Construction) (s)" data-action="animationSettings" data-actionval="animationSettings"></i>
</div>

<div id="fold-content" class="padded">
	<p>
		<strong>Welcome to the Prashu Framework!!!</strong>
	</p>
	
	<p>
		The current version is {PRASHU_VERSION}
	</p>
	
	<p>
		Hope you like it!
	</p>
	
	<p>
		Prashanta
	</p>
	
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	Devkota ji
</div>

<script type="text/javascript">
Prashu.addToBootChain(function(){
	Prashu.balloons.generate(9, {flgInfoBalloon:false, word:""});
	Prashu.balloons.startAnimation(50);
}.bind(this));
</script>