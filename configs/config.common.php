<?php 
/**
 * Configurations that are common across all ENVIRONMENTS
 */

define('PRASHU_VERSION', '1.0.1');

define('PRASHU_QSTRING', '?v=0.2019');
define('BOOTSTRAP_QSTRING', '?v=0.1');
define('JQUERY_QSTRING', '?v=0.1');
define('CAROUSEL_QSTRING', '');

// Now load ENVIRONMENT specific configs
require_once CONFIG_DIR . ENVIRONMENT . '/config.php';
