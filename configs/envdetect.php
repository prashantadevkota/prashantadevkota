<?php
/**
 * This file detects the environment -- whether in command line mode or HTTP mode.
 * You can add your configurations easily by simply adding values in the correct place
 */

// Computer Names used to detect the ENVIRONMENT in CLI_MODE
$cliComputerNames = 	array(
	'prod' => 		array(),
	'staging' =>	array(),
	'testing' =>	array(),
	'dev' => 		array('PRASHULAPTOP')	// Add more names as you wish
);

// Server Names used to detect the ENVIRONMENT in non CLI_MODE(HTTP)
$serverNames = 	array(
	'prod' => 		array(),
	'staging' =>	array(),
	'testing' =>	array(),
	'dev' => 		array('prashantadevkota.dev')	// Add more names as you wish
);

if(isCliMode())	// Command Line mode
{
	die("cmd");
	define('CLI_MODE', true);
	$computerName = 	isset($_SERVER['COMPUTERNAME']) ? $_SERVER['COMPUTERNAME'] : '';
	
	foreach($cliComputerNames as $key => $row)
	{
		if( array_search($computerName, $row, true) !== false )
		{
			define('ENVIRONMENT', $key);
			break;
		}
	}
		
	if(!defined('ENVIRONMENT'))
	{
		define('ENVIRONMENT', 'prod');
	}
}
else	// HTTP/HTTPS mode
{
	define('CLI_MODE', false);
	$httpHost = 	isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
	
	foreach($serverNames as $key => $row)
	{
		if( array_search($httpHost, $row, true) !== false )
		{
			define('ENVIRONMENT', $key);
			break;
		}
	}
	
	if(!defined('ENVIRONMENT'))
	{
		define('ENVIRONMENT', 'prod');
	}
}

// Environment Related Functions
function isCliMode()
{
	return !isset($_SERVER['HTTP_HOST']);
}
