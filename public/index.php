<?php
// The web root folder (i.e. the public folder)
define('WEBROOT_PRASHU', __DIR__ . '/');

// The project root folder
define('ROOTDIR_PRASHU', WEBROOT_PRASHU . '../');

// Prashu Engine folder
define('ENGINEDIR_PRASHU', ROOTDIR_PRASHU . 'frameworks/prashuengine/');

require_once ENGINEDIR_PRASHU . 'prashuengine.php';
