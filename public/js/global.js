/* Global JavaScript File */
window['Prashu'] = new PrashuClass();
function PrashuClass()
{
	var
		me = 		this,
		$ = 		pQuery,
		mailTo = 	null,
		alert = 	null,	/* Will point to our alert function */
		bootChain = [],
		
		$window = 		null,
		winHeight = 	0,
		winWidth = 		0,
		navbarHeight = 	70
		;
	
	me.data = 	{
			xsmall_screen: 	768,
			small_screen:	992,
			medium_screen: 	1200
		};

	me.boot = 	function(){
		createDOMElements();
		bindEvents();
		positionElements();

		var len = bootChain.length;
		for(var i=0; i<len; i++)
		{
			bootChain[i]();
		}
	}.bind(me);

	/* Constructor */
	(function __construct(){
		if(typeof jQuery !== 'undefined')
		{
			$ = jQuery;
		}
		
		if(typeof console === 'undefined' || typeof console.log === 'undefined')
		{
			console = 	{log: function(msg){alert(msg);} };
		}
		
		// OO Composition
		me.balloons = 	new PrashuBalloons($, me);

		/* Define our own Alert function */
		alert = 	function(msg){
			window['alert'](msg);
		};

		// On Document Ready
		$('document').ready(me.boot);
	})();
	// ********************************************
	
	me.addToBootChain = function(func){
		if(typeof func === 'function')
		{
			bootChain.push(func);
		}
	};
	
	var createDOMElements = function(){
		mailTo = 	$('<a id="prashuMailToATag" style="display:none;"></a>')[0];
		$('body').append(mailTo);
	};
	
	var bindEvents = function(){
		/**
		 * Window Resize
		 */
		$(window).resize(function() {
			me.balloons.resizeWindow();
			positionElements();
		});
		
		/** 
		 * Social Icons
		 */ 
		$(document).on('click', '.social-icon', function(e){
			e.stopImmediatePropagation();
			
			var 
				$elem = 		$(e.currentTarget),
				socialName = 	$elem.data('socialName')
				;
			
			socialName = 	typeof socialName === 'undefined' ? '' : socialName.toLowerCase();
			
			me.openSocialLink(socialName);
			return false;
		});
		/* */
		
		/**
		 * Actions
		 */
		$(document).on('click', '.action', function(e){
			e.stopImmediatePropagation();
			
			var 
				$elem = 		$(e.currentTarget),
				action = 		$elem.data('action') || '',
				actionVal = 	$elem.data('actionVal') || ''
				;
			
			processAction(action, actionVal);
			return false;
		});
		
		if($(".landing-page").length >0)	// Only if on the landing page
		{
			$(document).on('scroll', function(e){
				if($(document).scrollTop()<winHeight-80)
				{
					$(".prashu-nav").addClass('landing-page');
				}
				else
				{
					$(".prashu-nav.landing-page").removeClass('landing-page');
				}
			});
			
		}
		
		/**
		 * Keyboard Events
		 */
		$('body').on('keypress', function(e){
			var
				key = 	e.key
				;
			switch(key)
			{
				// Next Frame
				case "n":
					processAction('nextFrame', 'nextFrame');
					break;
				
				// Play
				case "p":
					processAction('playAnimation', 'playAnimation');
					break;
				
				// Pause
				case "u":
					processAction('pauseAnimation', 'pauseAnimation');
					break;
				
				// Stop
				case "o":
					processAction('stopAnimation', 'stopAnimation');
					break;
				
				// Previous Frame
				case "v":
					processAction('previousFrame', 'previousFrame');
					break;
				
				// Settings
				case "s":
					processAction('animationSettings', 'animationSettings');
					break;
			}
		});
	};
	
	var positionElements = function(){
		$window = 		$(window);
		winHeight = 	$window.height();
		winWidth = 		$window.width();
		
		var
			aniControlHeight = 		28,
			aniControlWidth = 		155,
			aniSettingsHeight = 	200
			;
		var
			left = 	Math.round(winWidth/2 - aniControlWidth/2),
			top = 	winHeight - aniControlHeight*4
			;
		
		$("#animation_control").css({left: left, top: top});
		$("#animation_settings").css({left: left, top: top-aniSettingsHeight });
		$("#fold_arrow").css({left: Math.round(winWidth/2)-36, top: Math.round(winHeight/2)-36});
	};
	
	var processAction = function(action, val){
		switch(action)
		{
			case 'email':
				return me.mailTo(val);
				
			case 'skype':
				return me.skype(val);
			
			case 'goBelowFold':
				return me.goBelowFold(val);
			
			//****************************************************
			case 'nextFrame':
				return me.balloons.nextFrame();
			
			case 'previousFrame':
				return me.balloons.previousFrame();

			case 'playAnimation':
				return me.balloons.playAnimation();
			
			case 'stopAnimation':
				return me.balloons.stopAnimation();
			
			case 'pauseAnimation':
				return me.balloons.pauseAnimation();
			
			case 'animationSettings':
				return me.balloons.toggleSettings();
			//****************************************************
		}
	};
	
	me.openSocialLink = function(socialName){
		alert(socialName);
	};
	
	me.mailTo = function(addr){
		mailTo.href = "mailto:" + addr;
		mailTo.click();
	};
	
	me.skype = function(addr){
		mailTo.href = "skype:" + addr;
		mailTo.click();
	};
	
	me.goBelowFold = function(){
		$('html, body').animate({
		    scrollTop: $("#fold-content").offset().top - navbarHeight
		 }, 1000);
		//$(document).scrollTop(winHeight);
	};
	
	/**
	 * Just a default value for '$'
	 */
	function pQuery()
	{
		/* Placeholder for the value of '$' until a JS library like jQuery is loaded */
	}
}

function PrashuBalloons($, parent)
{
	var
		me = 			this,
		numBalloons = 	27,
		balloons = 		[],
		timer = 		null,
		
		balloonSource = 	['red', 'green', 'blue', 'yellow', 'pink'],
		balloonSize = 		'medium',
		balloonHeight = 	0,	/* Calculated later */
		balloonWidth = 		0,	/* Calculated later */
		balloonWord = 		"Prashanta Prasad Devkota", /* Author of this Script */
		
		$window = 		$(window),
		winHeight = 	$window.height()-25,
		winWidth = 		$window.width()-25,
		
		/* X - Wind */
		xWindDuration = 			0,	/* In frames */
		xWindStrength = 			4.5, /* 1 - 9 */
		xWindStrengthDecrease = 	0,	/* Wind strength will decrease by this amount every frame */
		xWindDirection = 			1, /* +ve Left -> Right, -ve is the reverse */
		/* Y - Wind */
		yWindDuration = 			0,	/* In frames */
		yWindStrength = 			4.5, /* 1 - 9 */
		yWindStrengthDecrease = 	0,	/* Wind strength will decrease by this amount every frame */
		yWindDirection = 			1, /* +ve Left -> Right, -ve is the reverse */
		
		/* Animation */
		flgAnimationInProgress = 	false,
		aniSpeed = 					100,	/* In milliseconds */
		aniCallback = 				null,
		aniFrameNum = 				0,
		aniMaxFrameNum = 			10000	/* Maximum frames per animation */
		;
	
	me.resizeWindow = function(){
		winHeight = 	$window.height()-25;
		winWidth = 		$window.width()-25;
	};
	
	me.set = function(varName, val){
		if(typeof varName === 'undefined' || typeof val === 'undefined')
		{
			return false;
		}
		me[varName] = 	val;
	};
	
	/**
	 * 
	 * @param num: Number of balloons to generate. Pass 0 to detect screen size & set number accordingly.
	 * @param options:
	 * 		'flgInfoBalloon' (boolean):	Show the main(info-showing) balloon or not
	 * 		'word' (string): The string that is used to fill letters in the balloons with
	 */
	me.generate = 	function(num, options){
		numBalloons = 	typeof num === 'undefined' ? numBalloons : num;
		var
			options = 			typeof options !== 'object' ? {} : options,
			flgInfoBalloon = 	typeof options['flgInfoBalloon'] !== 'undefined' ? options['flgInfoBalloon'] : true
			;
		balloonWord = 	typeof options['word'] === 'string' ? options['word'] : balloonWord;

		doCalcs();
		
		var $body = 	$('body');
		for(var i=0; i<numBalloons; i++)
		{
			var $obj = 	$(getBalloonCode());
			$body.append($obj);
			
			balloons[i] = 	{obj: $obj, flgMain: false};
		}
		
		
		if(flgInfoBalloon)
		{
			// The Main Balloon that will show information
			$obj = 	$(getBalloonCode(true));
			$body.append($obj);
			balloons[i] = 	{obj: $obj, flgMain: true};
		}
		//console.log(balloons);
	};
	
	me.startAnimation = function(speed, finishCallback){
		if(flgAnimationInProgress)
		{
			console.log("Animation in progress, please wait till it is finished!");
			return false;
		}
		aniSpeed = 	typeof speed === 'number' ? speed : 100;
		
		// Set the callback for the animation complete event
		if(typeof finishCallback === 'function')
		{
			aniCallback = 	finishCallback;
		}
		else if(typeof window[finishCallback] === 'function')
		{
			aniCallback = 	window[finishCallback];
		}
		
		aniFrameNum = 				0;
		flgAnimationInProgress = 	true;
		timer = 					setInterval(animate, aniSpeed);
	};
	
	me.playAnimation = function(){
		if(flgAnimationInProgress)	// Was paused
		{
			timer = 	timer === null ? setInterval(animate, aniSpeed) : timer;
			return true;
		}
		
		// Was stopped
		$(".balloon").css('display','block');
		$(".balloon").css({top: winHeight});
		me.startAnimation(50);
	};
	
	me.pauseAnimation = function(){
		clearInterval(timer);
		timer = null;
	};
	
	me.stopAnimation = function(){
		clearInterval(timer);
		flgAnimationInProgress = 	false;
		timer = 	null;
		
		$(".balloon").css('display','none');
	};
	
	me.nextFrame = function(){
		animate();		
	};
	
	me.previousFrame = function(){
		animateBackwards();
	};
	
	me.toggleSettings = function(){
		console.log("Animation settings is currently under construction.");
		return true;
		
		$("#animation_settings").slideToggle();
	};
	
	var doCalcs = function(){
		// Check a few things
		if(winWidth < parent.data.medium_screen)
		{
			numBalloons = 	winWidth < parent.data.xsmall_screen ? 3 : 5;
		}
		
		var
			balloonImg = 	"/images/balloons/" + balloonSource[0] + "-" + balloonSize + ".png"
			;
		
		$("<img/>").attr('src', balloonImg).on('load',
			function(){
				balloonWidth = 		this.width;
				balloonHeight = 	this.height;
				
				$(".balloon-text").css({
					top: 	(balloonHeight *-1) + 'px',
					left:	balloonWidth/5 + 'px'
				});
			});
	};
	
	function animate()
	{
		aniFrameNum++;
		if(aniFrameNum > aniMaxFrameNum)	// Forcefully stop the animation
		{
			me.stopAnimation();
		}
		
		// Check for xwind
		if(xWindDuration <= 0)
		{
			generateXWind();
		}
		else
		{
			xWindDuration--;
		}
		
		// Check for ywind
		if(yWindDuration <= 0)
		{
			generateYWind();
		}
		else
		{
			yWindDuration--;
		}
		
		var len = 		balloons.length;
		var xwind = 	xWindDuration >0 ? (xWindStrength*xWindDirection) : 0;
		var ywind = 	yWindDuration >0 ? (yWindStrength*yWindDirection) : 0;
		var left, top;
		for(var i=0; i<len; i++)
		{
			var $obj = 	balloons[i]['obj'];
			if(balloons[i]['flgMain'] === true)	// The Main Balloon (Showing the Info)
			{
				left = 	parseFloat($obj.css('left')) + parseFloat($obj.data('incx'));
				top = 	parseFloat($obj.css('top')) + parseFloat($obj.data('incy'));
			}
			else	// Other Balloons
			{
				var incx = 	xwind + parseFloat($obj.data('incx'));
				var incy = 	ywind + parseFloat($obj.data('incy'));
				
				// Corrections(if needed)
				incx += Math.abs(incx) <1 ? 1*xWindDirection : 0;
				incy += Math.abs(incy) <1 ? 1*yWindDirection : 0;
				
				left = 	parseFloat($obj.css('left')) + incx;
				top = 	parseFloat($obj.css('top')) + incy;
			}
			
			if(left<0)	// Out of the viewport from the left
			{
				left = 	winWidth - balloonWidth;
			}
			else if(left+balloonWidth>=winWidth)	// Out of the viewport from the right
			{
				left = 	0;
			}
			
			if(top+balloonHeight<0)
			{
				top = 	winHeight;
			}
			// This is a helium balloon which rises by default
			else if(top+balloonHeight>winHeight && balloons[i]['flgMain'] !== true)
			{
				$obj.data( 'incy', -Math.abs(parseFloat($obj.data('incy'))) );
			}
			$obj.css({left: left, top: top});
		}
		
		// Update information display
		var xwindText = (xwind>0 ? '&rarr; ' : '&larr; ') + xWindStrength;
		var ywindText = (ywind>0 ? '&darr; ' : '&uarr; ') + yWindStrength;

		var xinfoText = ('xWind: ' + xwindText).substr(0,28);
		var yinfoText = ('yWind: ' + ywindText).substr(0,28);
		$(".main-balloon .balloon-main-text.xinfo").html(xinfoText);
		$(".main-balloon .balloon-main-text.yinfo").html(yinfoText);
		
		decreaseXWindStrength();
		decreaseYWindStrength();
	}
	
	function animateBackwards()
	{
		console.log('Previous Frame is Under construction');
		return true;
		
		aniFrameNum--;
		if(aniFrameNum <= 0)	// No more previous frames
		{
			return false;
		}
		
		// *****************************************************************************************
		// Pop out some global vars
		// *****************************************************************************************
		xWindDirection = 	windStorage[aniFrameNum]['x']['windDir'] *-1;	// Reverse direction
		yWindDirection = 	windStorage[aniFrameNum]['y']['windDir'] *-1;	// Reverse direction
		// *****************************************************************************************
		
		var 
			len = 		balloons.length,
			xwind = 	windStorage[aniFrameNum]['x']['wind'] *-1,	// Reverse direction
			ywind = 	windStorage[aniFrameNum]['y']['wind'] *-1,	// Reverse direction
			xincData = 	parseFloat($obj.data('incx')) *-1,	// Reverse direction
			yincData = 	parseFloat($obj.data('incy')) *-1,	// Reverse direction
			left,
			top
			;
		
		for(var i=0; i<len; i++)
		{
			var $obj = 	balloons[i]['obj'];
			if(balloons[i]['flgMain'] === true)	// The Main Balloon (Showing the Info)
			{
				left = 	parseFloat($obj.css('left')) + xincData;
				top = 	parseFloat($obj.css('top')) + yincData;
			}
			else	// Other Balloons
			{
				var 
					incx = 	xwind + xincData,
					incy = 	ywind + yincData
					;
				
				// Corrections(if needed)
				incx += Math.abs(incx) <1 ? 1*xWindDirection : 0;
				incy += Math.abs(incy) <1 ? 1*yWindDirection : 0;
				
				left = 	parseFloat($obj.css('left')) + incx;
				top = 	parseFloat($obj.css('top')) + incy;
			}
			
			if(left<0)	// Out of the viewport from the left
			{
				left = 	winWidth - balloonWidth;
			}
			else if(left+balloonWidth>=winWidth)	// Out of the viewport from the right
			{
				left = 	0;
			}
			
			if(top<0)
			{
				top = 	winHeight;
			}
			// This is a helium balloon which rises by default
			else if(top+balloonHeight>winHeight && balloons[i]['flgMain'] !== true)
			{
				$obj.data( 'incy', -Math.abs(parseFloat($obj.data('incy'))) );
			}
			$obj.css({left: left, top: top});
		}
		
		// Update information display
		var xwindText = (xwind>0 ? '&rarr; ' : '&larr; ') + xWindStrength;
		var ywindText = (ywind>0 ? '&darr; ' : '&uarr; ') + yWindStrength;

		var xinfoText = ('xWind: ' + xwindText).substr(0,28);
		var yinfoText = ('yWind: ' + ywindText).substr(0,28);
		$(".main-balloon .balloon-main-text.xinfo").html(xinfoText);
		$(".main-balloon .balloon-main-text.yinfo").html(yinfoText);
		
		decreaseXWindStrength();
		decreaseYWindStrength();
	}

	function decreaseXWindStrength()
	{
		if(xWindStrength>0)
		{
			xWindStrength -= 	xWindStrengthDecrease;
		}
	}
	
	function decreaseYWindStrength()
	{
		if(yWindStrength>0)
		{
			yWindStrength -= 	yWindStrengthDecrease;
		}
	}
	
	function generateXWind()
	{
		var
			flgWind = 		Math.round(Math.random()*100)%2 === 0,
			minDuration = 	flgWind ? 100 : 20	// In Animation Frames
			;
		
		xWindDuration = 	minDuration + Math.round( Math.random() *10);
		xWindStrength = 	flgWind ? Math.round(Math.random()*10) : 0; /* 1 - 9 */
		xWindDirection *= 	-1; /* +ve Left -> Right, -ve is the reverse */
		
		xWindStrengthDecrease = 	(xWindStrength-2)/xWindDuration;
	}
	
	function generateYWind()
	{
		var
			flgWind = 		Math.round(Math.random()*100)%2 === 0,
			minDuration = 	20 // No wind duration is 20 frames
			;
		
		// 80% chance of upwind, 20% chance of downwind
		yWindDirection = 	(Math.round(Math.random()*10) %5 === 0) ? 1 : -1;	// +ve Down -> Right, -ve Up
		yWindStrength = 	flgWind ? Math.round(Math.random()*10) : 0; /* 1 - 9 */
		
		// Calculate the wind duration
		yWindDuration = 	yWindStrength>0 ? (100 + Math.round( Math.random() *10)) : minDuration;
		if(yWindDirection === 1)	// Special Wind Strength & Duration for Downwards wind 
		{
			yWindDuration = 	40;
			yWindStrength = 	0.5 + Math.round(Math.random()*3);
		}
		// END_OF Calculate the wind duration
		
		yWindStrengthDecrease = 	(yWindStrength-2)/yWindDuration;
//console.log(yWindStrength, yWindDuration, yWindDirection);
	}
	
	function getBalloonCode(flgMain)
	{
		var 
			count = 		typeof this.count === 'undefined' ? (this.count = 1) : this.count,
			flgMain = 		typeof flgMain === 'undefined' ? false : flgMain == true,
			srcModBy = 		balloonSource.length,
			balloonImg = 	"/images/balloons/" + balloonSource[count%srcModBy] + "-" + balloonSize + ".png",
		
			mul = 	winWidth, // 1000,
			top = 	winHeight - balloonHeight,
			left = 	Math.round(Math.random() * mul),
			
			incx = 	1+Math.round(Math.random()*5), // >=1 && <6
			incy = 	-(1 + Math.round(Math.random()*5))	// >=1 && <6
			;
		
		// Left Correction (if required)
		left = 	(left + balloonWidth) > winWidth ? (winWidth - balloonWidth) : left;
		
		if(flgMain)	// Main Balloon
		{
			var	code = '<div class="balloon main-balloon" data-incx="0" data-incy="-1" style="top:' + winHeight + 'px; left:' + (winWidth+balloonWidth)/2 + 'px;">'
							+'<div>'
								+ '<img src="/images/balloons/orange.png" />'
								+ '<div class="balloon-main-text xinfo">Info</div>'
								+ '<img src="/images/balloons/orange.png" />'
							+'</div>'
							+'<div>'
								+ '<img src="/images/balloons/orange.png" />'
								+ '<div class="balloon-main-text yinfo">Info</div>'
								+ '<img src="/images/balloons/orange.png" />'
							+'</div>'
						+'</div>';
		}
		else
		{
			var	code = '<div class="balloon" data-incx="' + incx + '" data-incy="' + incy + '" style="top:' + top + 'px; left:' + left + 'px;">' 
							+ '<img src="' + balloonImg + '" style="display: block;" />'
							+ '<div class="balloon-text">' + balloonWord.substr(count-1,1) + '</div>'
						+'</div>';
		}
		
		this.count++;
		return code;
	}
}
