<?php
ini_set('memory_limit', '200M');

date_default_timezone_set('Australia/Melbourne');

// -----------------------------------------
// Pull the path arguments
$pathArgs = null;
foreach($_GET as $path => $val)	// Only take the first GET string
{
	$pathArgs = 	explode('/', $path);
	array_shift($pathArgs);	// Discard the 0th elem as it will be null
	break;
}

$pathArgs = 	$pathArgs === null ? array('main', 'index') : $pathArgs;
$pathArgs[1] = 	isset($pathArgs[1]) ? $pathArgs[1] : 'index';
// -----------------------------------------

// Include the global functions file
require_once ENGINEDIR_PRASHU . 'core/functions.php';		// Core functions
require_once ENGINEDIR_PRASHU . 'global-functions.php';		// Global functions

// Decide which PHP framework to load
if($pathArgs)
{
	switch($pathArgs[0])
	{
		case 'ci2':		// Codeigniter-2
			hideFrameworkLoadString('/ci2');
			require_once ROOTDIR_PRASHU . 'frameworks/ci2/index.php';
			exit;
			break;
	}
}

// Use the prashu PHP framework!
// ------------------------------------------------------
// Detect the Environment
require_once ROOTDIR_PRASHU . 'configs/envdetect.php';

define('CONTROLLER_DIR', ENGINEDIR_PRASHU . 'controllers/');
define('MODEL_DIR', ENGINEDIR_PRASHU . 'models/');
define('BUSINESSLOGIC_DIR', ROOTDIR_PRASHU . 'businesslogic/');
define('VIEW_DIR', ROOTDIR_PRASHU . 'views/');
define('CONFIG_DIR', ROOTDIR_PRASHU . 'configs/');
define('DOWNLOAD_DIR', ROOTDIR_PRASHU . 'downloads/');

// ------------------------------------------------------
// Load the configs
// ------------------------------------------------------
require_once CONFIG_DIR . 'config.common.php';

require_once ENGINEDIR_PRASHU . 'core/prashu.php';
require_once ENGINEDIR_PRASHU . 'core/error.php';
// ------------------------------------------------------

// Create a new Prashu Engine
$Prashu = 	new Prashu($pathArgs);
$Prashu->startRunning();	// Away we go!
