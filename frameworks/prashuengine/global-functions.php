<?php
// Put your global functions here
function echoPre($var, $returnText = false, $die = false)
{
    if ($returnText) {
        return print_r($var, true);
    }

    echo '<pre>', print_r($var, true), '</pre>';
    if ($die) {
        die;
    }
}

/**
 * Use this function if the debug_backtrace is too long to read
 * @param string $die
 */
function debug_backtrace_prashu($returnText = true, $die = false)
{
    $res = debug_backtrace(false);
    foreach ($res as &$row) {
        if (isset($row['args'])) {
            unset($row['args']);
        }
        if (isset($row['object'])) {
            unset($row['object']);
        }
    }

    if ($returnText) {
        return print_r($res, true);
    }

    echo '<pre>', print_r($res, true), '</pre>';

    if ($die) {
        die;
    }
}

function echoSanitized($val, $flgReturnText=false)
{
	// htmlentities should be enough for now; if not, simply update the sanitizing algorithm here.
	// Raw & possibly filtered input AND output sanitization is the recommended process.
	
	$val = 	htmlentities($val);
	
	if($flgReturnText)
	{
		return $val;
	}
	
	echo $val;
	return true;
}
