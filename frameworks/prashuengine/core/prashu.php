<?php 
if(!defined('ENGINEDIR_PRASHU')) exit('No direct script access allowed');

Class Prashu
{
	protected $pathArgs = 			array();
	protected $controller = 		'';
	protected $controllerPath = 	'';
	protected $method = 			'';
	//------------------------------------
	public $Error = 				null;
	//------------------------------------
	protected $models = 			array();
	protected $views = 				array();
	//------------------------------------
	protected $PageDisplayOutput = 	'';
	//------------------------------------
	public $flgSanitizeOutput = 		true;
	/**
	 * Prashu will skip output sanitization(even when the flag is set) for vars with this prefix
	 */
	public $SanitizeOutputSkipPrefix = 	'_TEMPLATE_';
	//------------------------------------
	
	public function __construct($pathArgs)
	{
		$this->pathArgs = 	$pathArgs;
		unset($GLOBALS['pathArgs']);
		
		$controller = 		$pathArgs[0];
		$method = 			$pathArgs[1];
		$controllerPath = 	CONTROLLER_DIR . "{$controller}.php";
		
		$this->controller =			$controller;
		$this->controllerPath =		$controllerPath;
		$this->method = 			$method;
	}
	
	public function startRunning()
	{
		if(!file_exists($this->controllerPath))
		{
			return $this->controllerNotFound();
		}
		
		require_once($this->controllerPath);
		$controller = 	$this->controller;
		$method = 		$this->method;

		if(!class_exists($controller))
		{
			return $this->controllerNotFound();
		}
		$obj = 	new $controller($this);
		if(!method_exists($obj, $method))
		{
			return $this->methodNotFound();
		}
		$obj->Engine = 	$this;
		$pathArgs = 	$this->pathArgs;
		return call_user_func_array(array($obj, $method), array_splice($pathArgs, 2));
	}
	
	public function controllerNotFound()
	{
		http_response_code(404);
		echo "404 Not Found";
		return false;
	}
	
	public function methodNotFound()
	{
		http_response_code(404);
		echo "404 Method Not Found";
		return false;
	}
	
	public function displayView($_viewPath_, $data=array(), $flgReturnText=false, $mainTemplate='templates/main')
	{
		$_viewFullPath_ = 	VIEW_DIR . $_viewPath_ . '.php';
		if(!file_exists($_viewFullPath_))
		{
			echo "Could not load view: '{$_viewPath_}'";
			return false;
		}
		
		$mainTemplateFullPath = 	$mainTemplate ? (VIEW_DIR . $mainTemplate . '.php') : '';
		if($mainTemplateFullPath && !file_exists($mainTemplateFullPath))
		{
			echo "Could not load main template: '{$mainTemplate}'";
			return false;
		}
		
		$res = 	ob_start();
		if(!$res)	// We couldn't start output buffering!
		{
			// Log into system log AND do something appropriate here. We don't want to display because PHP code will be visible
			echo "Display buffer error! Please contact support.";
			return false;
		}
		
		// Some Default Values
		$data['PAGE_TITLE'] = 	isset($data['PAGE_TITLE']) ? $data['PAGE_TITLE'] : '';
		
		// Extra the variables into the current symbol table but don't overwrite
		extract($data, EXTR_SKIP);
		
		//***************************************************************
		// Load the view(must be loaded before the main template)
		//***************************************************************
		require_once $_viewFullPath_;
		$_PAGE_CONTENT_909_ = 		ob_get_contents();
		ob_clean();
		
		$flgSanitizeOutput = 			$this->flgSanitizeOutput;
		$SanitizeOutputSkipPrefix = 	$this->SanitizeOutputSkipPrefix;
		$prefixLen = 					strlen($SanitizeOutputSkipPrefix);
		
		// Variable substitution for the view
		$matches = 		array();
		$res = 			preg_match_all('({.+?})', $_PAGE_CONTENT_909_, $matches, PREG_OFFSET_CAPTURE);
		foreach($matches[0] as $row)
		{
			$_varName_ = 		trim($row[0], '{}');
			$prefix = 		substr($_varName_, 0, $prefixLen);
			
			if(isset($$_varName_))	// If the variable is defined
			{
				// Skip output sanitization
				if(!$flgSanitizeOutput || $_varName_ === '_TEMPLATE_PAGE_CONTENT' || $prefix === $SanitizeOutputSkipPrefix)
				{
					$_PAGE_CONTENT_909_ = 	str_replace($row[0], $$_varName_, $_PAGE_CONTENT_909_);
				}
				else	// Do the output sanitization
				{
					$replaceVal = 			echoSanitized($$_varName_, true);
					$_PAGE_CONTENT_909_ = 	str_replace($row[0], $replaceVal, $_PAGE_CONTENT_909_);
				}
			}
			elseif( defined($_varName_) )	// If a constant is defined with the same name
			{
				// Note: If you have to sanitize your constants, you're probably doing it wrong
				$_PAGE_CONTENT_909_ = 	str_replace($row[0], constant($_varName_), $_PAGE_CONTENT_909_);
			}
		}
		//***************************************************************
		
		if(!$mainTemplateFullPath)	// If the main template isn't given
		{
			// STOP Output Buffering
			ob_end_clean();
			
			if($flgReturnText)
			{
				return $_PAGE_CONTENT_909_;
			}
			
			echo $_PAGE_CONTENT_909_;
			return true;
		}
		
		// We are going to move forward towards the main template. Do some preparations
		$_TEMPLATE_PAGE_CONTENT = 	$_PAGE_CONTENT_909_;
		unset($_PAGE_CONTENT_909_);
		
		//***************************************************************
		// Load the main template
		//***************************************************************
		require_once $mainTemplateFullPath;
		$mainTemplateOutput = 	ob_get_contents();

		// Variable substitution for the main template
		$matches = 	array();
		$res = 	preg_match_all('({.+?})', $mainTemplateOutput, $matches, PREG_OFFSET_CAPTURE);
		foreach($matches[0] as $row)
		{
			$_varName_ = 		trim($row[0], '{}');
			$prefix = 		substr($_varName_, 0, $prefixLen);
			if(isset($$_varName_))	// If the variable is defined
			{
				// Skip output sanitization
				if(!$flgSanitizeOutput || $_varName_ === '_TEMPLATE_PAGE_CONTENT' || $prefix === $SanitizeOutputSkipPrefix)
				{
					$mainTemplateOutput = 	str_replace($row[0], $$_varName_, $mainTemplateOutput);
				}
				else	// Do the output sanitization
				{
					$replaceVal = 			echoSanitized($$_varName_, true);
					$mainTemplateOutput = 	str_replace($row[0], $replaceVal, $mainTemplateOutput);
				}
			}
			elseif( defined($_varName_) )	// If a constant is defined with the same name
			{
				// Note: If you have to sanitize your constants, you're probably doing it wrong
				$mainTemplateOutput = 	str_replace($row[0], constant($_varName_), $mainTemplateOutput);
			}
		}
		//***************************************************************
		
		// STOP Output Buffering
		ob_end_clean();
		
		if($flgReturnText)
		{
			return $mainTemplateOutput;
		}
		
		echo $mainTemplateOutput;
		return true;
	}
	
	public function getInstance()
	{
		return $this;
	}
}

// ********************************************************************
Class PrashuModule
{
	public $Engine;
	
	public function __construct()
	{
		//
	}
}

Class Prashu_Controller extends PrashuModule
{
	public function __construct()
	{
		//
	}
}

Class Prashu_Model extends PrashuModule
{
	public function __construct()
	{
		//
	}
}
// ********************************************************************

// ********************************************************************
Interface LoggerBase
{
	/**
	 * Simply write the message to the output file
	 * @param mixed $message
	 */
	public function write($message);
	
	/**
	 * Write the message & print_r the given data to the output file
	 * @param string $message
	 * @param mixed $data
	 */
	public function writeDebug($message, $data); 
}

Class Log implements LoggerBase
{
	public function write($message)
	{
		//
	}
	
	public function writeDebug($message, $data)
	{
		//
	}
}
// ********************************************************************
