<?php 
/* Core Functions for the Prashu Framework */
if ( ! defined('ENGINEDIR_PRASHU')) exit('No direct script access allowed');

function hideFrameworkLoadString($str)
{
	if(isset($_SERVER['REDIRECT_QUERY_STRING']))
	{
		$_SERVER['REDIRECT_QUERY_STRING'] = str_replace($str, '', $_SERVER['REDIRECT_QUERY_STRING']);
	}
	
	if(isset($_SERVER['REDIRECT_URL']))
	{
		$_SERVER['REDIRECT_URL'] = 	str_replace($str, '', $_SERVER['REDIRECT_URL']);
	}

	if(isset($_SERVER['QUERY_STRING']))
	{
		$_SERVER['QUERY_STRING'] = 	str_replace($str, '', $_SERVER['QUERY_STRING']);
	}

	if(isset($_SERVER['REQUEST_URI']))
	{
		$_SERVER['REQUEST_URI'] = 	str_replace($str, '', $_SERVER['REQUEST_URI']);
	}
}
