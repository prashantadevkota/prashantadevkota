<?php if ( ! defined('ENGINEDIR_PRASHU')) exit('No direct script access allowed');

Class Main extends Prashu_Controller
{
	/**
	 * Example of a simple view
	 */
	public function index()
	{
		$this->Engine->displayView('welcome', array('version'=> '1.0.0'), false, 'templates/landing-main');
	}
	
	/**
	 * Manipulating a rendered view, i.e. replacing all newlines with <br/>
	 */
	public function changelog()
	{
		$output = 	$this->Engine->displayView('changelog', array('version'=> '1.0.0'), true, false );
		$output = 	str_replace(array(PHP_EOL, "\r\n"), '<br/>', $output);
		
		$data = 	array('PAGE_TITLE'=>'Change Log', '_TEMPLATE_PAGE_CONTENT'=> $output);
		$this->Engine->displayView('templates/main', $data, false, false);
	}
	
	public function docs()
	{
		$this->Engine->displayView('docs', array('PAGE_TITLE'=>'Documentation'));
	}
	
	public function about()
	{
		$this->Engine->displayView('about', array('PAGE_TITLE'=>'About Me'));
	}
	
	public function source()
	{
		$this->Engine->displayView('source-code', array('PAGE_TITLE'=>'Clone Source'));
	}
	
	public function techstack()
	{
		$this->Engine->displayView('tech-stack', array('PAGE_TITLE'=>'Technology Stack'));
	}
	
	public function resume()
	{
		$this->Engine->displayView('resume', array('PAGE_TITLE'=>'Resume'));
	}
	
	public function download($file=false)
	{
		switch($file)
		{
			case 'resume-docx':
				$fileType = 	'docx';
				$contentType = 	'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
				$file = 		'Resume-Prashanta-Prasad-Devkota.docx';
				break;
			case 'resume-pdf':
				$fileType = 	'pdf';
				$contentType = 	'application/pdf';
				$file = 		'Resume-Prashanta-Prasad-Devkota.pdf';
				break;
			
			default:
				$fileType = 	'';
				$contentType = 	'text';
				$file = 		'';
		}
		
		$filePath = 	DOWNLOAD_DIR . $file;
		$flgExists =	file_exists($filePath);
		
		if(!$file || !$flgExists)		// If the file wasn't found
		{
			header('Content-Type: text');
			header("Content-Disposition: attachment; filename=\"File Not Found.txt\"");	// Force download
			echo 'File Not Found';
			return false;
		}
		
		// Set the Headers
		header("Content-Type: {$contentType}");
		header("Content-Disposition: attachment; filename=\"{$file}\"");	// Force download
		
		readfile($filePath);
		return true;
	}
}
