<?php if ( ! defined('ENGINEDIR_PRASHU')) exit('No direct script access allowed');

Class Webconsole extends Prashu_Controller
{
	public function index()
	{
		$this->Engine->displayView('webconsole/index', array('PAGE_TITLE'=>'WebConsole'));
	}
}
